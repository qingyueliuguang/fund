package com.netty.websocket;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpServerCodec;
import io.netty.handler.codec.http.websocketx.WebSocketServerProtocolHandler;
import io.netty.handler.stream.ChunkedWriteHandler;

public class WebSocketServerInitializer extends ChannelInitializer<SocketChannel> {
    @Override
    protected void initChannel(SocketChannel socketChannel) throws Exception {
        ChannelPipeline pipeline = socketChannel.pipeline();
        //websocket基于http协议，需要http编码解码器
        pipeline.addLast("HttpServerCodec",new HttpServerCodec());
        //支持写大块数据的助手类, 以块的方式来写的处理器
        pipeline.addLast("ChunkedWriteHandler",new ChunkedWriteHandler());

        //当我们用POST方式请求服务器的时候，对应的参数信息是保存在message body中的,如果只是单纯的用
        // HttpServerCodec是无法完全的解析Http POST请求的，因为HttpServerCodec只能获取uri中参数，
        // 所以需要加上HttpObjectAggregator.http服务器把HttpObjectAggregator放入管道里。
        // HttpObjectAggregator会把多个消息转换为一个单一的FullHttpRequest或是FullHttpResponse。
        //netty是基于分段请求的，HttpObjectAggregator的作用是将请求分段再聚合,参数是聚合字节的最大长度
        pipeline.addLast("HttpObjectAggregator",new HttpObjectAggregator(1024*64));

        //下边的"/ws" 表示该处理器处理的websocketPaht的路径，例如 客户端连接时使用：ws://192.168.88.12/ws 才能被这个处理器处理，反之则不行
       //这个助手类可以处理握手动作，handshaking(close,ping,pong),ping+pong是一个心跳
        //对于这个助手类而言，数据都是以帧进行传输，不同的数据类型对应的帧也不同
        pipeline.addLast("WebSocketServerProtocolHandler",new WebSocketServerProtocolHandler("/wc"));

        pipeline.addLast(new ChatHandler());//自定义的handler


    }
}
