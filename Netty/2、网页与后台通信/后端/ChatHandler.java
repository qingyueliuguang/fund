package com.netty.websocket;

import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.channel.group.ChannelGroup;
import io.netty.channel.group.DefaultChannelGroup;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;
import io.netty.util.concurrent.GlobalEventExecutor;

import java.time.LocalDate;

//传输的载体是帧，为websocket定制的传输文本的对象，frame是消息载体
public class ChatHandler extends SimpleChannelInboundHandler<TextWebSocketFrame> {

    //用于记录和管理所有客户端的channel
    private static ChannelGroup clients=new DefaultChannelGroup(GlobalEventExecutor.INSTANCE);

    @Override
    protected void channelRead0(ChannelHandlerContext channelHandlerContext, 
                                TextWebSocketFrame textWebSocketFrame) throws Exception {
        String text = textWebSocketFrame.text();//获取客户端传过来的消息
        System.out.println("接收到的报表数据："+text);
        //websocket用TextWebSocketFrame对象插损胡数据，writeAndFlush的参数为TextWebSocketFrame对象
        for(Channel channel:clients){
            channel.writeAndFlush(new TextWebSocketFrame(
                    "服务器在"+ LocalDate.now()+"接收到消息，消息内容为："+text));
        }

        //这个方法与上面for循环的方法一致
        //clients.writeAndFlush(new TextWebSocketFrame(
        //      "服务器在"+ LocalDate.now()+"接收到消息，消息内容为："+text));
    }


    //当客户端连接上服务端之后，获取客户端的channel，放到ChannelGroup里面去管理
    @Override
    public void handlerAdded(ChannelHandlerContext ctx) throws Exception {
        clients.add(ctx.channel());
        super.handlerAdded(ctx);
    }

    //用户离开客户端，即浏览器关闭时触发。将channel移除
    @Override
    public void handlerRemoved(ChannelHandlerContext ctx) throws Exception {
        //当客户端的channel被移除，ChannelGroup会自动将它移除掉，相当于执行了：clients.remove(ctx.channel())
        super.handlerRemoved(ctx);
        System.out.println("id=>"+ctx.channel().id());
        //channel创建出来之后，系统会给他分配一个很长的字符串作为唯一的id，asLongText是长的id
        System.out.println("long_id=>"+ctx.channel().id().asLongText());
        //精简版的id，不能保证不与别的channel重复
        System.out.println("short_id=>"+ctx.channel().id().asShortText());

    }
}
