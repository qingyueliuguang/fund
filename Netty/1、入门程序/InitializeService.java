package service.impl;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.http.HttpServerCodec;


/**
 * 初始化器，channel注册后，会执行里面的初始化方法，就是逐个添加里面的助手类
 */

//继承ChannelInitializer类，我们通信使用的是SocketChannel的方式，泛型也写这个
public class InitializeService extends ChannelInitializer<SocketChannel>{
    @Override
    protected void initChannel(SocketChannel socketChannel) throws Exception {
        //通过socketChannel去获取相应的管道
        ChannelPipeline channelPipeline = socketChannel.pipeline();

        //添加在哪些位置.有addAfter()，addBefore()，addFirst()，addLast()
        //通过管道添加助手类。HttpServerCodec是netty自己提供的助手类，类似于拦截器。
        //HttpServerCodec可以在客户端编码和在服务端解码。它是一个编解码器
        channelPipeline.addLast("HttpServerCodec",new HttpServerCodec());


        //添加自定义的助手类，返回Hello Netty
        channelPipeline.addLast("MyHandler",new MyHandler());

    }
}
