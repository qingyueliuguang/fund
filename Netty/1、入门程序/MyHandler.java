package service.impl;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.codec.http.*;
import io.netty.util.CharsetUtil;

/**
 * 创建自定义的助手类
 */

//对于请求来讲，相当于入站，是Http请求，泛型用HttpObject
public class MyHandler extends SimpleChannelInboundHandler<HttpObject>{

    //从缓冲区读数据
    @Override
    protected void channelRead0(ChannelHandlerContext channelHandlerContext, HttpObject httpObject) throws Exception {
        //channelHandlerContext是当前上下文对象，可用于获取当前的Handler
        Channel channel = channelHandlerContext.channel();
        System.out.println(channel.remoteAddress());//打印客户端的远程地址

        //发送消息 NIO中消息的发送接收都是通过缓冲区进行的，不是直接操作的
        //先将要发送的消息复制到缓冲区， Unpooled是一个操作缓冲区的类
        ByteBuf buffer = Unpooled.copiedBuffer("你好啊，netty", CharsetUtil.UTF_8);

        //构建Http response,三个参数分别是Http协议的版本，响应码和响应的内容
        DefaultFullHttpResponse response =
                new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.OK, buffer);

        //返回的数据类型，图片，文字等
        response.headers().set(HttpHeaderNames.CONTENT_TYPE,"text/plain;charset=utf-8");

        //数据的长度
        response.headers().set(HttpHeaderNames.CONTENT_LENGTH,buffer.readableBytes());



        //将数据写到缓冲区并且刷出去
        channelHandlerContext.writeAndFlush(response);
    }
}
