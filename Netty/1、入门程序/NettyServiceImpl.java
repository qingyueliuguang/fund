package service.impl;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;

public class NettyServiceImpl {
    public static void main(String[] args) throws InterruptedException {
        EventLoopGroup parentGroup =null;
        EventLoopGroup childGroup=null;
        try {
            //先创建主从线程组
            //主线程组，只负责接受客户端的连接，不做任何事情，跟主人一样
            parentGroup = new NioEventLoopGroup();
            //从线程组，主线程组将任务丢给他，让它去完具体的任务，干活的
            childGroup = new NioEventLoopGroup();

            //ServerBootstrap是netty服务端的启动类
            ServerBootstrap serverBootstrap = new ServerBootstrap();

            serverBootstrap.group(parentGroup, childGroup) //设置线程组模型为主从线程组
                    .channel(NioServerSocketChannel.class)//channel类型为NIO双通道类型
                    .childHandler(new InitializeService());//自处理器，用于处理childGroup
            //启动server，设置8088位启动端口，并且设置为同步启动的方式
            ChannelFuture channelFuture = serverBootstrap.bind(8088).sync();//绑定时耗时操作，设置为同步，netty会一直等待8088端口启动完毕

            //channelFuture.channel()获取当前channel
            //服务端管道关闭的监听器并同步阻塞,直到channel关闭,线程才会往下执行,结束进程；
            //主线程执行到这里就 wait 子线程结束，子线程才是真正监听和接受请求的，子线程就是Netty启动的监听端口的线程；
            //即closeFuture()是开启了一个channel的监听器，负责监听channel是否关闭的状态，
            //如果未来监听到channel关闭了，子线程才会释放，syncUninterruptibly()让主线程同步等待子线程结果。
            channelFuture.channel().closeFuture().sync();
        }finally {
            //优雅地关闭两个线程组
            parentGroup.shutdownGracefully();
            childGroup.shutdownGracefully();
        }
    }
}
